create database university;
use university;

create table STUDENTS 
(
stno varchar(4),
name varchar(35),
addr varchar(35),
city varchar(20),
state varchar(2),
zip varchar(10),
primary key (stno));

insert into STUDENTS VALUES ('1011', 'Edwards P.David', '10 Red Rd,', 'Newton', 'MA', '02159');
insert into STUDENTS values ('2415', 'Grongan A. Mary', '8 Walnut St.', 'Malden ', 'MA', '02148');
insert into STUDENTS values ('2661', 'Mixon Leatha ', '100 School. ST.', 'Brookline', 'MA', '02146');
insert into STUDENTS values ('2890', 'McLane Sandy', '30 Cass Rd,', 'Boston', 'MA', '02122');
insert into STUDENTS values ('3442', 'Novak Roland', '42 Beacon St.', 'Nashua', 'NH', '03060');
insert into STUDENTS values ('3566', 'Pierce Richard', '70 Park St. ', 'Brokkline', 'MA', '02146');
insert into STUDENTS values ('4022', 'Prior Lorraine', '8 Beacon St.', 'Boston', 'MA', '02125');
insert into STUDENTS values ('5544', 'Rawlings Jerry ', '15 Pleasant Dr.', 'Boston', 'MA', '02115');
insert into STUDENTS values ('5571', 'Lewis Jerry ', '1 Main Rd.', 'Providence', 'RI', '02904');


create table INSTRUCTORS 
(
empno varchar(3),
name varchar(35),
rank varchar(25),
roomno varchar(2),
telno varchar(4),
primary key (empno));

insert into INSTRUCTORS values ('019', 'Evans Robert', 'Professor', '82', '7122');
insert into INSTRUCTORS values ('023', 'Exxon George', 'Professor', '90', '9100');
insert into INSTRUCTORS values ('056', 'Sawyer Kathy', 'Assoc.Prof.', '91', '5110');
insert into INSTRUCTORS values ('126', 'Davis William', 'Assoc. Prof.', '72', '5411');
insert into INSTRUCTORS values ('234', 'Will Samuel', 'Assist. Prof. ', '90', '7024');

create table COURSES
(
cno varchar(5),
cname varchar(35),
cr int,
cap int,
primary key (cno));

insert into COURSES values ('cs110', 'Introduction to Computong', 4, 120);
insert into COURSES values ('cs210', 'Computer Programming', 4, 100);
insert into COURSES values ('cs240', 'Computer Architecture', 3, 100);
insert into COURSES values ('cs310', 'Data Structure', 3, 60);
insert into COURSES values ('cs350', 'Higher Level Languages', 3, 50);
insert into COURSES values ('cs410', 'Software Engineering', 3, 40);
insert into COURSES values ('cs460', 'Graphics', 3, 30);

create table GRADES
(
stno varchar(4) references STUDENTS(stno),
empno varchar(3) references INSTRUCTORS(empno),
cno varchar(5)references COURSES(cno),
sem varchar(10),
year int,
grade int,
primary key (stno,cno,sem,year),
check (grade <= 100));

INSERT INTO GRADES VALUES ('1011', '019', 'cs110', 'Fall', 2001, 40);
INSERT INTO GRADES VALUES ('2661', '019', 'cs110', 'Fall', 2001, 80);
INSERT INTO GRADES VALUES ('3566', '019', 'cs110', 'Fall', 2001, 95);
INSERT INTO GRADES VALUES ('5544', '019', 'cs110', 'Fall ', 2001, 100);
INSERT INTO GRADES VALUES ('1011', '023', 'cs110', 'Spring', 2002, 75);
INSERT INTO GRADES VALUES ('4022', '023', 'cs110', 'Spring', 2002, 60);
INSERT INTO GRADES VALUES ('3566', '019', 'cs240', 'Spring', 2002, 100);
INSERT INTO GRADES VALUES ('5571', '019', 'cs240', 'Spring', 2002, 50);
INSERT INTO GRADES VALUES ('2415', '019', 'cs240', 'Spring', 2002, 100);
INSERT INTO GRADES VALUES ('3442', '234', 'cs410', 'Spring', 2002, 60);
INSERT INTO GRADES VALUES ('5571', '234', 'cs410', 'Spring', 2002, 80);
INSERT INTO GRADES VALUES ('1011', '019', 'cs210', 'Fall', 2002, 90);
INSERT INTO GRADES VALUES ('2661', '019', 'cs210', 'Fall', 2002, 70);
INSERT INTO GRADES VALUES ('3566', '019', 'cs210', 'Fall', 2002, 90);
INSERT INTO GRADES VALUES ('5571', '019', 'cs210', 'Spring', 2003, 85);
INSERT INTO GRADES VALUES ('4022', '019', 'cs210', 'Spring', 2003, 70);
INSERT INTO GRADES VALUES ('5544', '056', 'cs240', 'Spring', 2003, 70);
INSERT INTO GRADES VALUES ('1011', '056', 'cs240', 'Spring', 2003, 90);
INSERT INTO GRADES VALUES ('4022', '056', 'cs240', 'Spring', 2003, 80);
INSERT INTO GRADES VALUES ('2661', '234', 'cs310', 'Spring', 2003, 100);
INSERT INTO GRADES VALUES ('4022', '234', 'cs310', 'Spring', 2003, 75);

create table ADVISING
(
stno varchar(4) references STUDENTS(stno),
empno varchar(3) references INSTRUCTORS(empno),
primary key (stno));

insert into ADVISING values ('1011', '019');
insert into ADVISING values ('2415', '019');
insert into ADVISING values ('2661', '023');
insert into ADVISING values ('2890', '023');
insert into ADVISING values ('3442', '056');
insert into ADVISING values ('3566', '126');
insert into ADVISING values ('4022', '234');
insert into ADVISING values ('5544', '023');
insert into ADVISING values ('5571', '234');





